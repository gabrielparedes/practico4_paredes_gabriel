# Camino de datos y control del microprocesador
---------------------------------------------

 ## **Introducción**
 En este trabajo práctico se debe diseñar un datapath para un sub-conjunto del Set de
Instrucciones de RISC-V. Se proveen dibujos para el diseño teórico del procesador

 ## **Desarrollo**

1. Genere dibujos independientes para cada uno de los bloques que va a utilizar para
construir el procesador.

![bloques](bloques.png)


2. Realice el dibujo de un Datapath completo para el set de instrucciones propuesto.
Indique en el dibujo anchos de todos los buses y nombres de las señales intermedias que luego utilizará en el código.

![Datapath](Datapath.png)

3. Realice una tabla de verdad para todas las señales de control


|Entradas o Salidas | Nombre de Señal | R-formato |   Id   |   sd   |   beq   | 
| ----------------- | --------------- | --------- | ------ | ------ | ------- |
|    Entrada        |      I[6]       |      0    |    0   |   0    |    1    |
|    Entrada        |      I[5]       |      1    |    0   |   1    |    1    |
|    Entrada        |      I[4]       |      1    |    0   |   0    |    0    |
|    Entrada        |      I[3]       |      0    |    0   |   0    |    0    |
|    Entrada        |      I[2]       |      0    |    0   |   0    |    0    |
|    Entrada        |      I[1]       |      1    |    1   |   1    |    1    |
|    Entrada        |      I[0]       |      1    |    1   |   1    |    1    |
| ----------------- | --------------- | --------- | ------ | ------ | ------- |
|     Salida        |     ALU Src     |      0    |    1   |   1    |    0    |
|     Salida        |    Memto Reg    |      0    |    1   |   X    |    X    |
|     Salida        |    Reg Write    |      1    |    1   |   0    |    0    |
|     Salida        |     Mem Read    |      0    |    1   |   0    |    0    |
|     Salida        |    Mem Write    |      0    |    0   |   1    |    0    |
|     Salida        |     Branch      |      0    |    0   |   0    |    1    |
|     Salida        |     ALUOp1      |      1    |    0   |   0    |    0    |
|     Salida        |     ALUOp0      |      0    |    0   |   0    |    1    |

